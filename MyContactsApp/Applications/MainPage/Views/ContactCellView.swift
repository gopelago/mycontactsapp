//
//  ContactCellView.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/13/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import UIKit
import SDWebImage

class ContactCellView: UIView {

    @IBOutlet var contactProfileImage: UIImageView!
    @IBOutlet var contactNameLabel: UILabel!
    @IBOutlet var contactFavouriteImage: UIImageView!
    
    
    //
    
    func initCell(contact: ContactData?) {
        DispatchQueue.main.async {
            if let contact = contact {
                // Profile Image
                if let profilePicUrlString = contact.profilePic,
                        let profilePicUrl = URL(string: profilePicUrlString) {
                    if let image = SDImageCache.shared().imageFromDiskCache(forKey: profilePicUrl.absoluteString) {
                        self.contactProfileImage.image = image
                    } else {
                        self.contactProfileImage.sd_setImage(with: profilePicUrl,
                                                             placeholderImage: UIImage.init(named: "MissingContact"),
                                                             options: [.retryFailed, .progressiveDownload],
                                                             completed: { (image, error, cacheType, url) in
                                                                //
                        })
                    }
                }
                self.contactProfileImage.layer.cornerRadius = self.contactProfileImage.frame.height / 2
                self.contactProfileImage.layer.masksToBounds = false
                self.contactProfileImage.clipsToBounds = true
                
                // Name
                if let firstName = contact.firstName {
                    self.contactNameLabel.text = firstName
                }
                if let lastName = contact.lastName {
                    self.contactNameLabel.text = self.contactNameLabel.text?.appending(" \(lastName)")
                }
                
                // Favourite
                if contact.favorite {
                    self.contactFavouriteImage.image = UIImage(named: "Favorite")
                }
            }
        }
    }
    
}
