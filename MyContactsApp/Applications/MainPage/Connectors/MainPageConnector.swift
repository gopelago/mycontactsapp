//
//  MainPageConnector.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/13/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import UIKit
import ObjectMapper

class MainPageConnector: CommonConnectors {

    static let sharedInstance = MainPageConnector()


    //
    
    let kPathContacts = "contacts.json"
    let kPathContactsById = "contacts/{id}.json"
    
    
    //

    func getContacts(onCompletion: @escaping (Result<[Contact]>) -> Void) {
        let route = kPathContacts
        
        self.makeHTTPRequest(path: route, httpMethod: "GET", params: nil) { (success, data, error) in
            if success {
                if let data = Mapper<Contact>().mapArray(JSONObject: data) {
                    onCompletion(.success(data))
                }
            } else {
                onCompletion(.failure(error))
            }
        }
    }
    
}
