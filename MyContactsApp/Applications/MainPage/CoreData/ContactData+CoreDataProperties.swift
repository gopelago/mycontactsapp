//
//  ContactData+CoreDataProperties.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/14/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import Foundation
import CoreData


extension ContactData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ContactData> {
        return NSFetchRequest<ContactData>(entityName: "ContactData")
    }

    @NSManaged public var contactId: Int32
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var profilePic: String?
    @NSManaged public var favorite: Bool
    @NSManaged public var url: String?
    
    var letterSection: String? {
        let characters = firstName!.characters.map { String($0) }
        return characters[0].uppercased()
    }

}
