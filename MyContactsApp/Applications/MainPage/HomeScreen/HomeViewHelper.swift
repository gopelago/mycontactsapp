//
//  HomeViewHelper.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/12/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import UIKit
import CoreData

class HomeViewHelper: NSObject {
    
    weak var controller: CommonTableViewController?
    
    
    // MARK: - handleEvent(...)
    
    func didLoad() {
        
        //
        getContacts()
    }
    
    
    // MARK: - GeneralViewRouterDelegate
    
    func push(viewController: UIViewController) {
        controller?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    // MARK: - Core Data
    
    private func createContactEntity(contact: Contact) -> NSManagedObject? {
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        if let contactEntity = NSEntityDescription.insertNewObject(forEntityName: "ContactData", into: context) as? ContactData {
            contactEntity.contactId = Int32(contact.contactId!)
            contactEntity.firstName = contact.firstName
            contactEntity.lastName = contact.lastName
            contactEntity.profilePic = contact.profilePic
            contactEntity.favorite = contact.favorite
            contactEntity.url = contact.url
            
            return contactEntity
        }
        return nil
    }
    
    private func saveContactInCoreData(contacts: [Contact]) {
        _ = contacts.map{self.createContactEntity(contact: $0)}
        
        do {
            try CoreDataStack.sharedInstance.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    lazy var fetchedContactDataResultsController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: ContactData.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: true),
                                        NSSortDescriptor(key: "lastName", ascending: true)]

        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: CoreDataStack.sharedInstance.persistentContainer.viewContext,
                                                                  sectionNameKeyPath: nil, //"letterSection",
                                                                  cacheName: nil)
        fetchedResultsController.delegate = self
        
        return fetchedResultsController
    }()
    
    private func clearContactData() {
        do {
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: ContactData.self))
            do {
                let objects = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                CoreDataStack.sharedInstance.saveContext()
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
    
    
    // MARK: - API Call 
    
    func getContacts() {
        do {
            try self.fetchedContactDataResultsController.performFetch()
        } catch let error  {
            print("ERROR: \(error)")
        }
        
        controller?.view.lock()
        MainPageConnector.sharedInstance.getContacts { (result) in
            self.controller?.view.unlock()
            
            switch result {
            case .success(let object) :
                self.clearContactData()
                self.saveContactInCoreData(contacts: object)
                
                break
            case .failure(let error) :
                // Show alert
                let alertViewController = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .alert)
                self.controller?.present(alertViewController, animated: true, completion: nil)
                
                let when = DispatchTime.now() + 5
                DispatchQueue.main.asyncAfter(deadline: when) {
                    alertViewController.dismiss(animated: true, completion: nil)
                }
                
                break
            }
        }
    }
    
}

extension HomeViewHelper: CommonTableViewDelegate {

    
    // MARK: - CommonTableViewDelegate:UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    // MARK: - CommonTableViewDelegate
    
    func shouldShowSeparatorLine() -> Bool {
        return true
    }
    
    func shouldReload(tableView: UITableView) -> Bool {
        return false
    }
    
    func handleEvent(withControllerLifeCycle event: ViewControllerLifeCycleEventsEnum, viewController: CommonTableViewController, otherInfo:[String:Any]?) {
        if event == .didLoad {
            didLoad()
        }
    }
    
}

extension HomeViewHelper: CommonTableViewDataSource {
    
    // MARK: - CommonTableViewDataSource:UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var sections = 0
        
        if let count = fetchedContactDataResultsController.sections?.count {
            sections = count
        }
        
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        
        if let count = fetchedContactDataResultsController.sections?[section].numberOfObjects {
            rows = count
        }

        return rows
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return fetchedContactDataResultsController.sections?[section].name
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCellView", for: indexPath)
            as? BaseTableViewCell else {
                return UITableViewCell()
        }
        cell.selectionStyle = .none
        
        if let customView = cell.customView as? ContactCellView {
            customView.initCell(contact: fetchedContactDataResultsController.object(at: indexPath) as? ContactData)
        }
        
        return cell
    }
    
    
    // MARK: - CommonTableViewDataSource
    
    func reusableIdentifiers() -> [String]? {
        return [NSStringFromClass(UITableViewCell.self),
                "ContactCellView"]
    }
    
    func navigationTitle() -> String {
        return ""
    }
    
}


extension HomeViewHelper: NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            self.controller?.tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.controller?.tableView.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.controller?.tableView.endUpdates()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.controller?.tableView.beginUpdates()
    }
    
}

