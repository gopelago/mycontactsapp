//
//  Contact.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/13/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import UIKit
import ObjectMapper

class Contact: NSObject, Mappable {
    
    var contactId: Int?
    var firstName: String?
    var lastName: String?
    var profilePic: String?
    var favorite: Bool = false
    var url: String?
    
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    override init(){
        super.init()
    }
    
    func mapping( map: Map) {
        contactId   <- map["id"]
        firstName   <- map["first_name"]
        lastName    <- map["last_name"]
        profilePic  <- map["profile_pic"]
        favorite    <- map["favorite"]
        url         <- map["url"]
    }
    
}
