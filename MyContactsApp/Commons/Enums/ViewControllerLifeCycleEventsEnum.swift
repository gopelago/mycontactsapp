//
//  ViewControllerLifeCycleEventsEnum.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/12/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

enum ViewControllerLifeCycleEventsEnum: Int {
    
    case didLoad = 0
    case willAppear
    case didAppear
    case willDisappear
    case didDisappear
    case willTransition
    case deInit
    case didLayout
    case didReceiveMemoryWarning
    
}
