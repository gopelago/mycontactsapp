//
//  CommonConnectors.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/12/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import UIKit
import AFNetworking

typealias ServiceResponse = (Bool, Any?, Error?) -> Void

enum Result<T> : Error {
    case success(T)
    case failure(Error?)
}

class CommonConnectors: NSObject {

    static let parentConnector = CommonConnectors()

    let baseURL = "http://gojek-contacts-app.herokuapp.com/"
    
    
    //
    
    @discardableResult func makeHTTPRequest(path: String, httpMethod: String, params: Any? = [String:Any](), onCompletion: @escaping ServiceResponse) -> URLSessionDataTask? {
        let endPoint = baseURL.appending("\(path)")
        
        let configs : URLSessionConfiguration = getConfigs(endPoint: endPoint)
        let sessionManager = AFHTTPSessionManager.init(baseURL: URL.init(string: baseURL), sessionConfiguration: configs)
        
        let successHandler = { (task: URLSessionDataTask, responseObject: Any?) -> Swift.Void in
            onCompletion(true, responseObject, nil)
        }
        let errorHandler =  { (task: URLSessionDataTask?, error: Error?) -> Swift.Void  in
            onCompletion(false, nil, error)
        }

        if httpMethod == "GET" {
            return sessionManager.get(endPoint, parameters: params, progress: nil, success: successHandler, failure: errorHandler)
        }
        else if httpMethod == "POST" {
            return sessionManager.post(endPoint, parameters: params, progress: nil, success: successHandler, failure: errorHandler)
        }
        else if httpMethod == "PUT" {
            return sessionManager.put(endPoint, parameters: params, success: successHandler, failure: errorHandler)
        }
        
        return nil
    }
    
    func getConfigs(endPoint: String) -> URLSessionConfiguration {
        let configs: URLSessionConfiguration = URLSessionConfiguration.default
        configs.timeoutIntervalForRequest = 60
        configs.requestCachePolicy = .reloadIgnoringLocalCacheData
        configs.urlCache = nil
        return configs
    }
    
}
