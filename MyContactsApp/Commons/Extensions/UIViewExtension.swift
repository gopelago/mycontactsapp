//
//  UIViewExtension.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/14/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import UIKit

extension UIView {
    
    // MARK: - Lock View
    
    func isLocked() -> Bool {
        if let _ = viewWithTag(10) {
            return true
        }
        return false
    }
    
    func lock() {
        self.lock(nil)
    }
    
    func lock(_ navigationItem: UINavigationItem?) {
        self.lock(navigationItem, activityIndicator: true)
    }
    
    func lock(_ navigationItem: UINavigationItem?, activityIndicator: Bool) {
        self.lock(navigationItem, activityIndicator: activityIndicator, tapRecognizer: nil)
    }
    
    func lock(_ navigationItem: UINavigationItem?, activityIndicator: Bool, tapRecognizer: UITapGestureRecognizer?) {
        self.lock(navigationItem, activityIndicator: activityIndicator, tapRecognizer: tapRecognizer, alpha: 0.75)
    }
    
    func lock(_ navigationItem: UINavigationItem?, activityIndicator: Bool, tapRecognizer: UITapGestureRecognizer?, alpha: CGFloat) {
        if let _ = viewWithTag(10) {
            //View is already locked
        } else {
            let lockView = UIView(frame: bounds)
            lockView.backgroundColor = UIColor(white: 0.0, alpha: alpha)
            lockView.tag = 10
            lockView.alpha = 0.0
            lockView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            if tapRecognizer != nil {
                lockView.addGestureRecognizer(tapRecognizer!)
            }
            
            if activityIndicator == true {
                let activity = UIActivityIndicatorView(activityIndicatorStyle: .white)
                activity.hidesWhenStopped = true
                activity.center = lockView.center
                
                lockView.addSubview(activity)
                activity.startAnimating()
            }
            addSubview(lockView)
            
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 1.0
            })
        }
    }
    
    func unlock() {
        self.unlock(nil)
    }
    
    func unlock(_ navigationItem: UINavigationItem?) {
        if let lockView = viewWithTag(10) {
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 0.0
            }, completion: { finished in
                lockView.removeFromSuperview()
            })
        }
    }
    
}
