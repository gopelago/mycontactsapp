//
//  BaseTableViewCell.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/12/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    var customView : UIView?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override public init(style: UITableViewCellStyle, reuseIdentifier: String?){
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.initializeCustomView(identifier: reuseIdentifier)
    }
    
    func initializeCustomView(identifier:String?){
        if let id = identifier {
            if let tempView = (Bundle.main.loadNibNamed(id, owner: self, options: nil)?[0] as? UIView) {
                customView = tempView
                
                contentView.addSubview(tempView)
                customView?.translatesAutoresizingMaskIntoConstraints = false
                
                contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":tempView]))
                contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":tempView]))
            } else {
                assertionFailure("Are you sure \(id) class exists in your project? ( Check Target membership once )")
            }
        } else {
            assertionFailure("You just passed nil identifier while registering BM_BaseTableViewCell")
        }
        
    }
    
    public init(style: UITableViewCellStyle, customViewReuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: customViewReuseIdentifier)
        
        self.initializeCustomView(identifier: customViewReuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        if let customViewForCell = customView as? BaseTableViewCellProtocol {
            customViewForCell.prepareForReuse()
        }
    }
    
}

protocol BaseTableViewCellProtocol {
    func prepareForReuse()
}
