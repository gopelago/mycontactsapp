//
//  CommonTableViewController.swift
//  MyContactsApp
//
//  Created by Jimmy Julius Harijanto on 9/12/17.
//  Copyright © 2017 jimzland. All rights reserved.
//

import UIKit

class CommonTableViewController: UIViewController {
    
    //
    @IBOutlet weak var tableView: UITableView!
    
    //
    var dataSource: CommonTableViewDataSource?
    var delegate: CommonTableViewDelegate?
    
    
    // MARK: - Overridden Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //
        setTableViewDataSourceAndDelegate()
        
        // tableView's properties
        //        tableView.estimatedRowHeight = 300
        tableView.backgroundColor = UIColor.groupTableViewBackground
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        setBackgroundColorForTableView()
        if let flag = self.delegate?.shouldShowSeparatorLine(), !flag {
            tableView.separatorStyle = .none
        } else {
            tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20)
        }
        
        reloadData()
        handleEvent(event: .didLoad)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isTranslucent = false
        
        if let title = dataSource?.navigationTitle() {
            self.title = title
        }
        self.setNavigationView()
        
        handleEvent(event: .willAppear)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        handleEvent(event: .didAppear)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        handleEvent(event: .willDisappear)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        handleEvent(event: .didDisappear)
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        let dict = ["newCollection": newCollection, "coordinator": coordinator] as [String: Any]
        handleEvent(event: .willTransition, otherInfo: dict)
        
        self.adjustTableViewContentInset()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        handleEvent(event: .didReceiveMemoryWarning)
    }
    
    deinit {
        handleEvent(event: .deInit)
    }

    
    // MARK: - Custom Methods
    
    func reloadData() {
        if let _ = delegate?.shouldReload(tableView: tableView) {
            tableView.reloadData()
        }
    }
    
    func handleEvent(event: ViewControllerLifeCycleEventsEnum, otherInfo: [String:Any]? = nil) {
        self.delegate?.handleEvent(withControllerLifeCycle: event, viewController: self, otherInfo: otherInfo)
    }
    
    func refreshTableview(_ notification: Notification) {
        tableView.reloadData()
    }
    
    
    //
    
    func setBackgroundColorForTableView() {
        if let color = self.dataSource?.backgroundColorForTableView() {
            self.tableView.backgroundColor = color
        }
    }
    
    func setNavigationView() {
        if let navigationCustomView = self.dataSource?.getNavigationView(for: self) {
            navigationCustomView.translatesAutoresizingMaskIntoConstraints = false
            navigationCustomView.tag = 9999
            
            self.navigationController?.view.addSubview(navigationCustomView)
            
            let views = ["navigationCustomView":navigationCustomView]
            let widthConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[navigationCustomView]|", options: NSLayoutFormatOptions(rawValue: UInt(0)), metrics: nil, views: views)
            
            self.navigationController?.view.addConstraints(widthConstraints)
            self.navigationController?.view.addConstraint(NSLayoutConstraint.init(item: navigationCustomView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.navigationController?.view, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: 0.0))
            self.navigationController?.view.addConstraint(NSLayoutConstraint.init(item: navigationCustomView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant:80.0))
        }
    }
    
    func adjustTableViewContentInset() {
        if let _ = dataSource?.getNavigationView(for: self) {
            if let height = self.navigationController?.navigationBar.frame.size.height{
                let diff = (80 - height - 20)
                self.tableView.contentInset = UIEdgeInsetsMake(diff, 0, 0, 0)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setTableViewDataSourceAndDelegate(){
        tableView.dataSource = dataSource
        tableView.delegate = delegate
        
        if let ids = dataSource?.reusableIdentifiers() {
            for id in ids {
                self.tableView.register(BaseTableViewCell.self, forCellReuseIdentifier:id)
            }
        }
        if let ids = dataSource?.reusableNibsIdentifier() {
            for id in ids {
                tableView.register(UINib(nibName: id, bundle: nil), forCellReuseIdentifier: id)
            }
        }
    }
    
}


//

protocol CommonTableViewDataSource: UITableViewDataSource {
    
    func reusableIdentifiers() -> [String]?
    func reusableNibsIdentifier() -> [String]?
    func headerFooterViewReusableIdentifiers() -> [String]?
    
    func navigationTitle() -> String
    
    func getNavigationView(for viewController: CommonTableViewController) -> UIView?
    func backgroundColorForTableView() -> UIColor?
    
}

extension CommonTableViewDataSource {
    
    func reusableIdentifiers() -> [String]? { return nil }
    func reusableNibsIdentifier() -> [String]? { return nil }
    func headerFooterViewReusableIdentifiers() -> [String]? { return nil }
    
    func getNavigationView(for viewController: CommonTableViewController) -> UIView? { return nil }
    func backgroundColorForTableView() -> UIColor? { return UIColor.groupTableViewBackground }
    
}


//

protocol CommonTableViewDelegate: UITableViewDelegate {
    
    func shouldShowSeparatorLine() -> Bool
    func shouldReload(tableView: UITableView) -> Bool
    func handleEvent(withControllerLifeCycle event: ViewControllerLifeCycleEventsEnum, viewController:CommonTableViewController, otherInfo: [String:Any]?)
    
}

extension CommonTableViewDelegate {
    
    func handleEvent(withControllerLifeCycle event: ViewControllerLifeCycleEventsEnum, viewController:CommonTableViewController, otherInfo: [String:Any]?) { }
    
}
